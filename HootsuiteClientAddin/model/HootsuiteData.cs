﻿using System;
using System.Collections.Generic;

namespace ININ.Alliances.HootsuiteClientAddin.model
{
    public class HootsuiteData
    {
        public string Version { get; set; }
        public HootsuitePost Post { get; set; }

        public HootsuiteData()
        {
            Post = new HootsuitePost();
        }
    }

    public class HootsuitePost
    {
        public string Href { get; set; }
        public string Id { get; set; }
        public DateTime Datetime { get; set; }
        public string Source { get; set; }
        public string Network { get; set; }
        public HootsuitePostContent Content { get; set; }
        public HootsuitePostUser User { get; set; }
        public List<HootsuitePostAttachments> Attachments { get; set; }
        public List<HootsuitePostConversation> Conversation { get; set; }
        public string Reply { get; set; }

        public HootsuitePost()
        {
            Datetime = DateTime.MinValue;
            Content = new HootsuitePostContent();
            User = new HootsuitePostUser();
            Attachments = new List<HootsuitePostAttachments>();
            Conversation = new List<HootsuitePostConversation>();
        }
    }

    public class HootsuitePostContent
    {
        public string Body { get; set; }
        public string BodyHtml { get; set; }

        public HootsuitePostContent()
        {
            // An error message of sorts
            Body = "Invalid Body!";
            BodyHtml = "Invalid HTML Body!";
        }
    }

    public class HootsuitePostUser
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
    }

    public class HootsuitePostAttachments
    {
        public string Type { get; set; }
        public string Url { get; set; }
        public string Thumbnail { get; set; }
        public string Title { get; set; }
        public HootsuiteAttachmentItems Items { get; set; }
    }

    public class HootsuiteAttachmentItems
    {
        public string Target { get; set; }
        public string ThumbnailSrc { get; set; }
    }

    public class HootsuitePostConversation
    {
        public string UserId { get; set; }
        public DateTime Datetime { get; set; }
        public string Likes { get; set; }
        public string Name { get; set; }
        public string ProfileUrl { get; set; }
        public string Uid { get; set; }
        public string Text { get; set; }

        public HootsuitePostConversation()
        {
            Datetime = DateTime.MinValue;
        }
    }
}