﻿using ININ.IceLib.Connection;
using ININ.IceLib.Interactions;
using ININ.InteractionClient.AddIn;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms;

namespace ININ.Alliances.HootsuiteClientAddin.viewModel
{
    public class AddinViewModel : ViewModelBase
    {
        #region Private Members

        private ObservableCollection<InteractionViewModel> _interactions = new ObservableCollection<InteractionViewModel>();
        private readonly IServiceProvider _serviceProvider;
        private IQueue _queue;
        private IInteractionSelector _interactionSelector;
        private InteractionViewModel _selectedInteraction;
        private Session _session;
        private InteractionsManager _interactionsManager;

        #endregion

        #region Public Members

        public ObservableCollection<InteractionViewModel> Interactions
        {
            get { return _interactions; }
            set { _interactions = value; }
        }

        public InteractionViewModel SelectedInteraction
        {
            get { return _selectedInteraction; }
            set
            {
                _selectedInteraction = value; 
                OnPropertyChanged();
            }
        }

        #endregion

        public AddinViewModel(IServiceProvider provider)
        {
            _serviceProvider = provider;
            Initialize();
        }

        #region Private Methods

        private void Initialize()
        {
            try
            {
                // Get IceLib session to perform actions on interactions (i.e. disconnect)
                _session = ININ.InteractionClient.ServiceLocator.Current.GetInstance<Session>();
                _interactionsManager = InteractionsManager.GetInstance(_session);

                // Set up queue watch
                var queueService = _serviceProvider.GetService(typeof(IQueueService)) as IQueueService;
                _queue = queueService.GetMyInteractions(new[]
                {
                    "Eic_InteractionId",
                    "Hootsuite_Subject",
                    "Hootsuite_Priority",
                    "Hootsuite_Reason",
                    "Hootsuite_Notes",
                    "Hootsuite_RawData",
                    "Hootsuite_Reply",
                    "Hootsuite_ReplyWorkgroup",
                    "Hootsuite_Attachments",
                    InteractionAttributes.State,
                    InteractionAttributes.StateDisplay
                });

                // Subscribe to queue events
                _queue.InteractionAdded += Queue_OnInteractionAdded;
                _queue.InteractionChanged += Queue_OnInteractionChanged;
                _queue.InteractionRemoved += Queue_OnInteractionRemoved;
                
                // Subscribe to Selected Interaction Changed event (when the agent selects a different interaction)
                _interactionSelector = _serviceProvider.GetService(typeof(IInteractionSelector)) as IInteractionSelector;
                if (_interactionSelector != null)
                {
                    _interactionSelector.SelectedInteractionChanged += InteractionSelector_OnSelectedInteractionChanged;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
#if DEBUG
                MessageBox.Show("Initialize: " + ex.Message);
#endif
            }
        }

        private void Queue_OnInteractionAdded(object sender, ININ.InteractionClient.AddIn.InteractionEventArgs e)
        {
            try
            {
                Context.Send(s =>
                {
                    try
                    {
                        // Make sure it's not already disconnected
                        var state = e.Interaction.GetAttribute(InteractionAttributes.State);
                        if (state.Equals(InteractionAttributeValues.State.ExternalDisconnect, StringComparison.InvariantCultureIgnoreCase) ||
                            state.Equals(InteractionAttributeValues.State.InternalDisconnect, StringComparison.InvariantCultureIgnoreCase))
                            return;

                        // Verify that this interaction has hootsuite data
                        if (string.IsNullOrEmpty(e.Interaction.GetAttribute("Hootsuite_RawData"))) return;

                        // Add it to the list if it was not there
                        if (!Interactions.Any(i => i.InteractionId == e.Interaction.InteractionId))
                        {
                            Interactions.Add(new InteractionViewModel(e.Interaction));
                        }

                        // Select it if we don't have anything else
                        if (SelectedInteraction == null)
                            SelectedInteraction = Interactions[Interactions.Count - 1];
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
#if DEBUG
                        MessageBox.Show("Queue_OnInteractionAdded (Context.Send): " + ex.Message);
#endif
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
#if DEBUG
                MessageBox.Show("Queue_OnInteractionAdded: " + ex.Message);
#endif
            }
        }

        private void Queue_OnInteractionChanged(object sender, ININ.InteractionClient.AddIn.InteractionEventArgs e)
        {
            try
            {
                Context.Send(s =>
                {
                    try
                    {
                        // Find the interaction
                        var interaction = Interactions.FirstOrDefault(i => i.InteractionId.Equals(e.Interaction.InteractionId));
                        if (interaction == null)
                        {
                            // Verify that this interaction has hootsuite data
                            if (string.IsNullOrEmpty(e.Interaction.GetAttribute("Hootsuite_RawData"))) return;

                            // Add it to the list
                            interaction = new InteractionViewModel(e.Interaction);
                            if (!Interactions.Any(i => i.InteractionId == e.Interaction.InteractionId))
                            {
                                Interactions.Add(interaction);
                            }
                        }
                        
                        // The addin doesn't tell us what attributes changed, so we have to do a blanket update
                        interaction.RaiseChangedNotifications();
                        
                        if (!String.IsNullOrEmpty(interaction.Reply))
                        {
                            //Transfer interaction to the "Reply Workgroup" to wait for the reply to be seen by the Community Manager(s)
                            var iceInteraction = _interactionsManager.CreateInteraction(new InteractionId(interaction.InteractionId));
                            iceInteraction.SetStringAttribute("HootSuite_Replied", "True");
                            iceInteraction.BlindTransferAsync(new QueueId(QueueType.Workgroup, interaction.ReplyWorkgroup), onTransferComplete, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
#if DEBUG
                        MessageBox.Show("Queue_OnInteractionChanged (Context.Send): " + ex.Message);
#endif
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
#if DEBUG
                MessageBox.Show("Queue_OnInteractionChanged: " + ex.Message);
#endif
            }
        }

        private void Queue_OnInteractionRemoved(object sender, ININ.InteractionClient.AddIn.InteractionEventArgs e)
        {
            try
            {
                Context.Send(s =>
                {
                    try
                    {
                        // Find the interaction
                        var interaction = Interactions.FirstOrDefault(i => i.InteractionId.Equals(e.Interaction.InteractionId));
                        if (interaction == null) return;

                        // Remove from our list
                        Interactions.Remove(interaction);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
#if DEBUG
                        MessageBox.Show("Queue_OnInteractionRemoved (Context.Send): " + ex.Message);
#endif
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
#if DEBUG
                MessageBox.Show("Queue_OnInteractionRemoved: " + ex.Message);
#endif
            }
        }

        private void onTransferComplete(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                // Transfer failed. Show why.
                MessageBox.Show("On Transfer Complete: " + e.Error.Message);
            }
        }

        private void InteractionSelector_OnSelectedInteractionChanged(object sender, EventArgs e)
        {
            try
            {
                Context.Send(s =>
                {
                    try
                    {
                        // If it's nothing, skip
                        if (_interactionSelector.SelectedInteraction == null) return;

                        // Find the interaction
                        var interaction = Interactions.FirstOrDefault(i => i.InteractionId.Equals(_interactionSelector.SelectedInteraction.InteractionId));

                        // Skip if we didn't find the interaction (it was probably a non-hootsuite interaction like a call or chat)
                        if (interaction == null) return;

                        // Set it as the selected interaction
                        SelectedInteraction = interaction;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
#if DEBUG
                        MessageBox.Show("InteractionSelector_OnSelectedInteractionChanged (Context.Send): " + ex.Message);
#endif
                    }
                }, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
#if DEBUG
                MessageBox.Show("InteractionSelector_OnSelectedInteractionChanged: " + ex.Message);
#endif
            }
        }

        #endregion
    }
}
