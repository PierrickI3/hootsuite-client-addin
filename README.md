# Hootsuite Interaction Client/Desktop Addin #

This add-in is a fork of an existing (private at the time of this writing) add-in hosted in the Interactive Intelligence's repository.

This Interaction .Net Client/Desktop addin goes hand-in-hand with the [Hootsuite Plugin](https://bitbucket.org/PierrickI3/hootsuite-plugin) integration between CIC and Hootsuite.com.
For more information, check [Social Interactions](https://icsocialinteractions.wordpress.com/new).

### What is it for? ###

* Shows the content of generic object attributes containing Social Media information and allows agents to reply.
* Replies are not sent directly back to the social platform. They are shown in an Hootsuite App Stream to allow Community Managers to format a proper reply.
* 1.0.0.0

### How do I get set up? ###

* Fork this
* Open in Visual Studio (2013)
* Change (or disable) the <PostBuildEvent> property (path to deploy the addin to your local instance of the Interaction Client or Desktop application) in the HootsuiteClientAddin.csproj file (or in Visual Studio/Project Properties/Build)
* Build
* Run your client application
  * Using Interaction .Net Client:
    * Click on the area next to "My Interactions" and select "New View". Then select the "Hootsuite" addin in the "Hootsuite" category.
  * Using Interaction Desktop:
    * Click on File/New/View and select the "Hootsuite" view.
* You can then re-order the view as you'd like

![alt tag](https://bytebucket.org/PierrickI3/hootsuite-client-addin/raw/7e8ff99ed1c55a5f061940c7b54040c27e81e938/InteractionDesktopView.jpg)

### Contribution guidelines ###

Please share your updates as much as possible. I always welcome feedback, whether good or bad!

### Who do I talk to? ###

* Pierrick Lozach, From Interactive Intelligence, Inc.