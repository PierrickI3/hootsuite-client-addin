﻿using ININ.Alliances.HootsuiteClientAddin.viewModel;
using ININ.InteractionClient.AddIn;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HootSuiteClientAddin.Tests
{
    [TestClass]
    public class InteractionUnitTests
    {
        [TestMethod]
        public void NullInteractionShouldHaveNullProperties()
        {
            IInteraction interaction = null;
            var interactionViewModel = new InteractionViewModel(interaction);
            Assert.IsNull(interactionViewModel.Name);
        }

        [TestMethod]
        public void NullInteractionShouldHaveNullHootsuiteData()
        {
            IInteraction interaction = null;
            var interactionViewModel = new InteractionViewModel(interaction);
            Assert.IsNull(interactionViewModel.HootsuiteData.UserName);
        }

        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void NullInteractionShouldCauseExceptionWhenTryingToGetAnAttribute()
        {
            IInteraction interaction = null;
            var interactionViewModel = new InteractionViewModel(interaction);
            Assert.IsNull(interactionViewModel.Priority);
        }

        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void NullInteractionShouldCauseReplyException()
        {
            IInteraction interaction = null;
            var interactionViewModel = new InteractionViewModel(interaction);
            interactionViewModel.ReplyToMessage(null);
        }

        [TestMethod]
        public void ShouldHaveReplyButtonContent()
        {
            IInteraction interaction = null;
            var interactionViewModel = new InteractionViewModel(interaction);
            Assert.IsNotNull(interactionViewModel.ReplyButtonContent);
        }

        [TestMethod]
        public void ShouldNotBeAbleToReply()
        {
            IInteraction interaction = null;
            var interactionViewModel = new InteractionViewModel(interaction);
            Assert.IsFalse(interactionViewModel.CanExecute);
        }
    }
}
