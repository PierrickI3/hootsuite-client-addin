﻿using System;
using System.Runtime;
using System.Runtime.InteropServices;
using ININ.Alliances.HootsuiteClientAddin.model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HootSuiteClientAddin.Tests
{
    [TestClass]
    public class HootSuiteNetworkTests
    {
        [TestMethod]
        public void ShouldContainFacebook()
        {
            Assert.IsTrue(Enum.IsDefined(typeof(HootsuiteNetwork), HootsuiteNetwork.Facebook));
        }

        [TestMethod]
        public void ShouldContainTwitter()
        {
            Assert.IsTrue(Enum.IsDefined(typeof(HootsuiteNetwork), HootsuiteNetwork.Twitter));
        }

        [TestMethod]
        public void ShouldContainUnknown()
        {
            Assert.IsTrue(Enum.IsDefined(typeof(HootsuiteNetwork), HootsuiteNetwork.Unknown));
        }
    }
}
