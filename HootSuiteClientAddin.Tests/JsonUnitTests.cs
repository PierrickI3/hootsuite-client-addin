﻿using System;
using System.ComponentModel;
using ININ.Alliances.HootsuiteClientAddin;
using ININ.Alliances.HootsuiteClientAddin.model;
using ININ.Alliances.HootsuiteClientAddin.viewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace HootSuiteClientAddin.Tests
{
    [TestClass]
    public class JsonUnitTests
    {
        [TestMethod]
        public void CanConvertTweetToJson()
        {
            var data = JsonConvert.DeserializeObject<HootsuiteData>("{\"post\":{\"network\":\"twitter\",\"href\":\"https://twitter.com/PNationale/status/612933075668963328\",\"id\":\"612933075668963328\",\"datetime\":\"2015-06-22T10:40:07.000Z\",\"source\":\"TweetDeck\",\"counts\":{\"likes\":3,\"shares\":11,\"replies\":null},\"content\":{\"body\":\"[RECRUTEMENT] #GardienDeLaPaix Vs avez manqué l'inscription en ligne? Envoyez votre dossier par courrier avt le 26 http://t.co/rebdOamvP8\",\"bodyhtml\":\"[RECRUTEMENT] #GardienDeLaPaix Vs avez manqué l'inscription en ligne? Envoyez votre dossier par courrier avt le 26 http://t.co/rebdOamvP8\"},\"user\":{\"userid\":847396122,\"username\":\"PNationale\"},\"attachments\":null,\"conversation\":null},\"profile\":{\"network\":\"TWITTER\",\"created_at\":\"Wed Sep 26 13:25:42 +0000 2012\",\"description\":\"Bienvenue sur le compte officiel de la Police nationale (conseils, prévention, actus, reportages, interactivité) Notre vocation, c'est vous!\",\"followers_count\":51787,\"friends_count\":296,\"id\":847396122,\"id_str\":\"847396122\",\"lang\":\"fr\",\"listed_count\":510,\"location\":\"\",\"name\":\"Police Nationale\",\"profile_image_url\":\"http://pbs.twimg.com/profile_images/557207403481927680/mY9Zkwxm_normal.jpeg\",\"profile_image_url_https\":\"https://pbs.twimg.com/profile_images/557207403481927680/mY9Zkwxm_normal.jpeg\",\"screen_name\":\"PNationale\",\"statuses_count\":2577,\"time_zone\":\"Paris\",\"url\":\"http://t.co/KD6vpxfIzs\",\"utc_offset\":7200,\"verified\":true}}");
            Assert.IsTrue(data.Post.Id.Equals("612933075668963328"));
        }

        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void ShouldFailToCanConvertTweetToJson()
        {
            var data = JsonConvert.DeserializeObject<HootsuiteData>("{\"post\":{\"network\":\"twitter\",\"href\":\"https://twitter.com/PNationale/status/612933075668963328\",\"id2\":\"612933075668963328\",\"datetime\":\"2015-06-22T10:40:07.000Z\",\"source\":\"TweetDeck\",\"counts\":{\"likes\":3,\"shares\":11,\"replies\":null},\"content\":{\"body\":\"[RECRUTEMENT] #GardienDeLaPaix Vs avez manqué l'inscription en ligne? Envoyez votre dossier par courrier avt le 26 http://t.co/rebdOamvP8\",\"bodyhtml\":\"[RECRUTEMENT] #GardienDeLaPaix Vs avez manqué l'inscription en ligne? Envoyez votre dossier par courrier avt le 26 http://t.co/rebdOamvP8\"},\"user\":{\"userid\":847396122,\"username\":\"PNationale\"},\"attachments\":null,\"conversation\":null},\"profile\":{\"network\":\"TWITTER\",\"created_at\":\"Wed Sep 26 13:25:42 +0000 2012\",\"description\":\"Bienvenue sur le compte officiel de la Police nationale (conseils, prévention, actus, reportages, interactivité) Notre vocation, c'est vous!\",\"followers_count\":51787,\"friends_count\":296,\"id\":847396122,\"id_str\":\"847396122\",\"lang\":\"fr\",\"listed_count\":510,\"location\":\"\",\"name\":\"Police Nationale\",\"profile_image_url\":\"http://pbs.twimg.com/profile_images/557207403481927680/mY9Zkwxm_normal.jpeg\",\"profile_image_url_https\":\"https://pbs.twimg.com/profile_images/557207403481927680/mY9Zkwxm_normal.jpeg\",\"screen_name\":\"PNationale\",\"statuses_count\":2577,\"time_zone\":\"Paris\",\"url\":\"http://t.co/KD6vpxfIzs\",\"utc_offset\":7200,\"verified\":true}}");
            Assert.IsTrue(data.Post.Id.Equals("612933075668963328"));
        }

        [TestMethod]
        public void CanConvertFacebookPostToJson()
        {
            var data = JsonConvert.DeserializeObject<HootsuiteData>("{\"post\":{\"network\":\"facebook\",\"href\":\"http://www.facebook.com/531008650/posts/10153089052753651\",\"id\":\"531008650_10153089052753651\",\"datetime\":\"2015-07-24T19:19:02.000Z\",\"source\":\"\",\"counts\":{\"likes\":3,\"replies\":5,\"shares\":0,\"views\":0},\"content\":{\"body\":\"Un petit moelleux au chocolat cœur caramel au beurre salé en dessert\",\"bodyhtml\":\"Un petit moelleux au chocolat cœur caramel au beurre salé en dessert\"},\"user\":{\"userid\":\"531008650\",\"username\":\"Pierrick Lozach\"},\"attachments\":null,\"conversation\":null},\"profile\":{\"network\":\"FACEBOOK\",\"first_name\":\"Pierrick\",\"gender\":\"male\",\"id\":\"531008650\",\"last_name\":\"Lozach\",\"link\":\"http://www.facebook.com/531008650\",\"locale\":\"en_US\",\"name\":\"Pierrick Lozach\",\"picture\":\"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/111…8aba890906&oe=5649E9EE&__gda__=1446903099_c4818e5ed70bd8c39bbfefaa5a294841\"}}");
            Assert.IsTrue(data.Post.Id.Equals("531008650_10153089052753651"));
        }

        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void ShouldFailToConvertFacebookPostToJson()
        {
            var data = JsonConvert.DeserializeObject<HootsuiteData>("{\"post\":{\"network\":\"facebook\",\"href\":\"http://www.facebook.com/531008650/posts/10153089052753651\",\"id2\":\"531008650_10153089052753651\",\"datetime\":\"2015-07-24T19:19:02.000Z\",\"source\":\"\",\"counts\":{\"likes\":3,\"replies\":5,\"shares\":0,\"views\":0},\"content\":{\"body\":\"Un petit moelleux au chocolat cœur caramel au beurre salé en dessert\",\"bodyhtml\":\"Un petit moelleux au chocolat cœur caramel au beurre salé en dessert\"},\"user\":{\"userid\":\"531008650\",\"username\":\"Pierrick Lozach\"},\"attachments\":null,\"conversation\":null},\"profile\":{\"network\":\"FACEBOOK\",\"first_name\":\"Pierrick\",\"gender\":\"male\",\"id\":\"531008650\",\"last_name\":\"Lozach\",\"link\":\"http://www.facebook.com/531008650\",\"locale\":\"en_US\",\"name\":\"Pierrick Lozach\",\"picture\":\"https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p200x200/111…8aba890906&oe=5649E9EE&__gda__=1446903099_c4818e5ed70bd8c39bbfefaa5a294841\"}}");
            Assert.IsTrue(data.Post.Id.Equals("531008650_10153089052753651"));
        }
    }
}
