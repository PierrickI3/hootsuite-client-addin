﻿using System;
using ININ.Alliances.HootsuiteClientAddin.model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HootSuiteClientAddin.Tests
{
    [TestClass]
    public class HootSuitePostTests
    {
        [TestMethod]
        public void ShouldHaveDefaultValues()
        {
            var hootSuitePost = new HootsuitePost();
            Assert.IsTrue(hootSuitePost.Datetime.Equals(DateTime.MinValue));
            Assert.IsNotNull(hootSuitePost.Content);
            Assert.IsNotNull(hootSuitePost.User);
            Assert.IsNotNull(hootSuitePost.Conversation);
            Assert.IsNotNull(hootSuitePost.Attachments);
        }
    }
}
